#include <stdbool.h>
#include "coord.h"

struct coord_s {
    int row;
    int col;
};

coord *coord_new() {
    coord *cd = calloc(sizeof(coord));
    return cd;
}

coord *coord_copy(coord *cd) {
    coord *cd_copy = malloc(sizeof (coord));
    memcpy(cd_copy, cd, sizeof (coord));
    return (cd_copy);
}

void coord_destroy(coord *cd) {
    free(cd);
}

int coord_get_row(coord *cd) {
    return (cd->row);
}

void coord_set_row(coord *cd, int row) {
    cd->row = row;
}

int coord_get_col(coord *cd) {
    return (cd->col);
}

void coord_set_col(coord *cd, int col) {
    cd->col = col;
}

void coord_get_coords(coord *cd, int *row, int *col) {
    *row = cd->row;
    *col = cd->col;
}

void coord_set_coords(coord *cd, int row, int col) {
    cd->row = row;
    cd->col = col;
}