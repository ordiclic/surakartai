#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "board.h"
#include "shifting.h"
#include "stack_shifting.h"
#include "coord.h"
#include "stack_coord.h"
#include "game_funcs.h"
#include "misc_funcs.h"

bool dist_is_one_between(int f_row, int f_col, int t_row, int t_col) {
    int delta_x = f_row - t_row;
    int delta_y = f_col - t_col;
    if (delta_x * delta_x < 2 && delta_y * delta_y < 2) {
        return (true);
    }
    return (false);
}

bool is_starting_position(shifting *cursor, direction orig_dir, direction cur_dir) {
    /*
        printf("is_starting_position([%d][%d]-[%d][%d]-%s==%s?)",
                cursor->f_row, cursor->f_col, cursor->t_row, cursor->t_col,
                dir_to_str(orig_dir), dir_to_str(cur_dir));
     */
    int *f_row, *f_col, *t_row, *t_col;
    shifting_get_all_coords(cursor, f_row, f_col, t_row, t_col);
    return (f_col == t_col && f_row == t_row && orig_dir == cur_dir);
}

bool advance_cursor(direction *dir, shifting *sh) {
#ifdef REALLY_DEBUG
    printf("advance_cursor(%s : [%d][%d])\n", dir_to_str(*dir),
            sh->t_row, sh->t_col);
#endif
    /*
     This function is intended to see what coordinates would be reached if
     we consider we can go in the dir direction. It's only dependant of the
     board, no checks for pieces presence are done. When taken, loops are
     indicated in sh_cursor.
     We're assuming an input with valid coordinates.
     IN-OUT : DIRECTION *dir, shifting sh_cursor
     OUT    : BOOL
     */

    int t_row, t_col;
    int pos;

    shifting_get_ending(sh, &t_row, &t_col);
    pos = 6 * t_row + t_col;

    switch (*dir) {
        case UP:
            switch (pos) {
                case 1:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 1, 0);
                    *dir = RIGHT;
                    return (true);
                case 2:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 2, 0);
                    *dir = RIGHT;
                    return (true);
                case 3:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 2, 5);
                    *dir = LEFT;
                    return (true);
                case 4:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 1, 5);
                    *dir = LEFT;
                    return (true);
                case 0:
                    return (false);
                case 5:
                    return (false);
                default:
                    t_row--;
                    shifting_set_ending(t_row, t_col);
                    return (true);
            }
        case DOWN:
            switch (pos) {
                case 31:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 4, 0);
                    *dir = RIGHT;
                    return (true);
                case 32:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 3, 0);
                    *dir = RIGHT;
                    return (true);
                case 33:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 3, 5);
                    *dir = LEFT;
                    return (true);
                case 34:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 4, 5);
                    *dir = LEFT;
                    return (true);
                case 30:
                    return (false);
                case 35:
                    return (false);
                default:
                    t_row++;
                    shifting_set_ending(t_row, t_col);
                    return (true);
            }
        case LEFT:
            switch (pos) {
                case 6:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 0, 1);
                    *dir = DOWN;
                    return (true);
                case 12:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 0, 2);
                    *dir = DOWN;
                    return (true);
                    ;
                case 18:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 5, 2);
                    *dir = UP;
                    return (true);
                case 24:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 5, 1);
                    *dir = UP;
                    return (true);
                    ;
                case 0:
                    return (false);
                case 30:
                    return (false);
                default:
                    t_col--;
                    shifting_set_ending(t_row, t_col);
                    return (true);
            }
        case RIGHT:
            switch (pos) {
                case 11:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 0, 4);
                    *dir = DOWN;
                    return (true);
                case 17:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 0, 3);
                    *dir = DOWN;
                    return (true);
                    ;
                case 23:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 5, 3);
                    *dir = UP;
                    return (true);
                case 29:
                    shifting_set_got_through_loop(sh, true);
                    shifting_set_ending(sh, 5, 4);
                    *dir = UP;
                    return (true);
                    ;
                case 5:
                    return (false);
                case 35:
                    return (false);
                default:
                    t_col++;
                    shifting_set_ending(t_row, t_col);
                    return (true);
            }
        default:
            printf("WTF PLS.\n");
            return (false);
    }
    return (true);
}

void get_attacks_pos(gamedata *data, coord *case_p, bool *attacking, bool *attacked) {
#ifdef REALLY_DEBUG
    printf("get_attacks_pos([%d][%d])\n", case_p->row, case_p->col);
#endif
    shifting cursor = shifting_new();
    board *gameboard = gamedata_get_board(data);
    color attacker_color = board_get_from_coords(gameboard, *case_p);
    int f_row, f_col;
    shifting_get_starting(cursor, &f_row, &f_col);
    for (int i = 0; i <= 3; i++) {
        int t_row, t_col;
        direction dir = directions_capture[i];
        direction orig_dir = directions_capture[i];
        shifting_set_ending(f_row, f_col);
        shifting_set_is_attack_move(cursor, false);
        while (advance_cursor(&dir, cursor)) {
            if (is_starting_position(cursor, orig_dir, dir)) {
                break; // It eventually happens if no piece is encountered.
            }
            shifting_get_ending(cursor, &t_row, &t_col);
            color color_p = board_get_from_indexes(gameboard, t_row, t_col);
            if (color_p != EMPTY) {
                // We have a surakarta piece there, and it will block any
                // further progression in the way.
                if (shifting_get_is_attack_move(cursor) && color_p != attacker_color) {
                    attacking[6 * f_row + f_col] = true;
                    attacked[6 * t_row + t_col] = true;
                }
                break;
            }
        }
    }
    shifting_destroy(cursor);
}

void get_attacks(gamedata data, coord case_p, stack_shifting ** attacks) {
#ifdef DEBUG
    printf("get_attacks([%d][%d])\n", case_p->row, case_p->col);
#endif
    /*
    This functions calculates where can a given piece attack.
     IN  : P data, SQUARE case_p
     OUT : 
     */
    shifting cursor = malloc(sizeof (struct shifting_s));
    int f_row = case_p->row;
    int f_col = case_p->col;
    color attacker_color = data->board[IDX(f_row, f_col)];
    cursor->f_row = f_row;
    cursor->f_col = f_col;
    for (int i = 0; i <= 3; i++) {
        direction dir = directions_capture[i];
        direction orig_dir = directions_capture[i];
        cursor->t_row = f_row;
        cursor->t_col = f_col;
        cursor->through_loop = false;
        while (advance_cursor(&dir, cursor)) {
            if (is_starting_position(cursor, orig_dir, dir)) {
                break; // It eventually happens if no piece is encountered.
            }
            color color_p = data->board[IDX(cursor->t_row, cursor->t_col)];
            if (color_p == attacker_color) {
                break;
            }
            if (color_p != EMPTY) {
                // We have a surakarta piece there, and it will block any
                // further progression in the way.
                if (cursor->through_loop == true && color_p != attacker_color) {
                    shifting sh = malloc(sizeof (struct shifting_s));
                    sh->f_row = f_row;
                    sh->f_col = f_col;
                    sh->t_row = cursor->t_row;
                    sh->t_col = cursor->f_col;
                    sh->is_attack_move = true;
                    stack_shifting_push(attacks, sh);
#ifdef DEBUG
                    print_surakarta(data);
                    printf("NEW ATTACK: [%d][%d] (%s) -> [%d][%d] (%s)\n",
                            f_row, f_col, color_to_str(attacker_color),
                            cursor->t_row, cursor->t_col, color_to_str(color_p)
                            );
#endif
                }
                break;
            }
        }
    }
    free(cursor);
}

int game_is_won(gamedata data) {
    /*
    This functions tells if the game is won by any of the two players.
    If the data->player_color player wins, it returns 1.
    If the other player wins, it returns -1.
    Otherwise it returns 0.
    We're assuming at least one piece remains in the game.
    
    IN  : P data
    OUT : INT
     */
    bool is_there_white = false;
    bool is_there_black = false;
    color *b = data->board;
    for (int i = 0; i < 36; i++) {
        if (b[i] == WHITE) {
            if (is_there_black) {
                return (0);
            }
            is_there_white = true;
        } else if (b[i] == BLACK) {
            if (is_there_white) {
                return (0);
            }
            is_there_black = true;
        }
    }
    if (is_there_white && data->current_player_color == WHITE) {
        return (1);
    }
    return (-1);
}

stack_coord *positions(gamedata data, color c) {
    /*
        printf("positions()\n");
     */
    /*
    This functions return all positions of pieces of the c color.

    IN  : P
    OUT : STACK SQUARE square_f
     */
    stack_coord *s_coords = stack_init_coord();
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            color case_p = data->board[IDX(i, j)];
            if (case_p == c) {
                coord coord_p = malloc(sizeof (struct coord_s));
                coord_p->row = i;
                coord_p->col = j;
                stack_push_coord(&s_coords, coord_p);
            }
        }
    }
    return (s_coords);
}

void add_simple_legal_moves(gamedata data, coord s_from, stack_shifting ** all_moves) {
    /*
    This function calculates the twelve (at most) possible legal moves from a
    piece and adds them in a stack passed as parameter.
     */

    color *board = data->board;
    int f_row = s_from->row;
    int f_col = s_from->col;
    for (int i = 0; i < 8; i++) {
        int *translation = (int *) directions_move[i];
        int t_row = f_row + translation[0];
        int t_col = f_col + translation[1];
        if (t_row >= 0 && t_col >= 0 && t_row < 6 && t_col < 6) {
            color t_color = board[IDX(t_row, t_col)];
            if (t_color == EMPTY) {
                if (dist_is_one_between(f_row, f_col, t_row, t_col)) {
                    shifting shift = malloc(sizeof (struct shifting_s));
                    shift->f_row = f_row;
                    shift->f_col = f_col;
                    shift->t_row = t_row;
                    shift->t_col = t_col;
                    shift->is_attack_move = false;
#ifdef DEBUG
                    printf("ADDING SIMPLE MOVE: [%d][%d] (%s) -> [%d][%d] (%s)\n",
                            f_row, f_col, color_to_str(board[IDX(f_row, f_col)]),
                            t_row, t_col, color_to_str(board[IDX(t_row, t_col)])
                            );
#endif
                    stack_shifting_push(all_moves, shift);
                }
            }
        }
    }
}

void get_next_moves(gamedata data, stack_shifting ** all_moves) {
    /*
        printf("surakarta_to_next_coords()\n");
     */
    /*
    This function gets all the available moves from the current board.
    IN    : P data
    IN-OUT: STACK SHIFTING all_moves
     */
    stack_coord *current_player_pieces = positions(data, data->current_player_color);
    while (!stack_is_empty_coord(current_player_pieces)) {
        coord cds = stack_pop_coord(&current_player_pieces);
        // First we get attack moves, as they are more prone to cutoffs.
        add_simple_legal_moves(data, cds, all_moves);
        free(cds);
    }
    current_player_pieces = positions(data, data->current_player_color);
    while (!stack_is_empty_coord(current_player_pieces)) {
        coord cds = stack_pop_coord(&current_player_pieces);
        // First we get attack moves, as they are more prone to cutoffs.
        get_attacks(data, cds, all_moves);
        free(cds);
    }
    free(current_player_pieces);
}

void undo_turn(gamedata data) {
    shifting prev_turn = stack_shifting_pop(&data->previous_moves);
#ifdef DEBUG
    printf("undo_turn: [%d][%d]->[%d][%d]\n", prev_turn->f_row, prev_turn->f_col,
            prev_turn->t_row, prev_turn->t_col);
#endif

    int row_i = prev_turn->f_row;
    int col_i = prev_turn->f_col;
    int row_f = prev_turn->t_row;
    int col_f = prev_turn->t_col;

    data->board[IDX(row_i, col_i)] = prev_turn->color_from;
    data->board[IDX(row_f, col_f)] = prev_turn->color_to;

    if (data->current_player_color == BLACK) {
        data->current_player_color = WHITE;
    } else {
        data->current_player_color = BLACK;
    }
    free(prev_turn);
}

void next_turn(gamedata data, shifting one_move) {
    /*
    Takes one move gotten from the surakarta_to_next_coords function,
    and generates the related surakarta board for further calculations.
    It edits the surakarta board and the color of the player.
    The move is assumed to be valid.
    
    IN     : SHIFTING one_move
    IN-OUT : P data
     */

#ifdef DEBUG
    printf("next_turn: [%d][%d] (%s) -> [%d][%d] (%s) [%s]\n",
            one_move->f_row, one_move->f_col, color_to_str(one_move->color_from),
            one_move->t_row, one_move->t_col, color_to_str(one_move->color_to),
            ((one_move->is_attack_move) ? "ATTACK" : "MOVE")
            );
#endif

    color *brd = data->board;

    int f_row = one_move->f_row;
    int f_col = one_move->f_col;
    int t_row = one_move->t_row;
    int t_col = one_move->t_col;

    shifting move = malloc(sizeof (struct shifting_s));
    move->f_row = f_row;
    move->f_col = f_col;
    move->t_row = t_row;
    move->t_col = t_col;
    move->color_from = brd[IDX(f_row, f_col)];
    move->color_to = brd[IDX(t_row, t_col)];
    move->is_attack_move = one_move->is_attack_move;

    brd[IDX(t_row, t_col)] = brd[IDX(f_row, f_col)];
    brd[IDX(f_row, f_col)] = EMPTY;

    stack_shifting_push(&data->previous_moves, move);

    if (data->current_player_color == BLACK) {
        data->current_player_color = WHITE;
    } else {
        data->current_player_color = BLACK;
    }
}

bool is_legal_move(gamedata data, shifting sh) {
#ifdef REALLY_DEBUG
    printf("is_legal_move: [%d][%d]->[%d][%d]\n", sh->f_row, sh->f_col, sh->t_row, sh->t_col);
#endif
    int f_row = sh->f_row;
    int f_col = sh->f_col;
    int t_row = sh->t_row;
    int t_col = sh->t_col;
    color c_origin = data->board[IDX(f_row, f_col)];
    color c_enemy = (c_origin == WHITE) ? BLACK : WHITE;
    color c_dest = data->board[IDX(t_row, t_col)];

    if (dist_is_one_between(f_row, f_col, t_row, t_col)
            && c_dest == EMPTY
            && sh->is_attack_move == false
            ) {
        return (true);
    }

    shifting cursor = malloc(sizeof (struct shifting_s));
    cursor->f_row = f_row;
    cursor->f_col = f_col;
    for (int i = 0; i < 4; i++) {
        cursor->t_row = f_row;
        cursor->t_col = f_col;
        cursor->through_loop = false;
        direction dir = directions_capture[i];
        direction orig_dir = directions_capture[i];
        while (advance_cursor(&dir, cursor)) {
            color t_color = data->board[IDX(cursor->t_row, cursor->t_col)];
            if (is_starting_position(cursor, orig_dir, dir)) {
                break; // We made a full loop w/o encountering another piece
            } else if (t_color != EMPTY) {
                // If we find another piece, the loop is going to be broken
                if (cursor->t_row == f_row
                        && cursor->t_col == f_col
                        && cursor->through_loop == true
                        && t_color == c_enemy
                        ) {
                    // If we went through a loop and got to the desired location
                    return (true);
                }
                break;
            }
        }
    }
    free(cursor);
    return (false);
}
