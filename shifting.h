#ifndef SHIFTING_H
#define	SHIFTING_H

#include "struct_funcs.h"
#include "coord.h"

typedef struct shifting_s shifting;

shifting *shifting_new();
shifting *shifting_copy(shifting *sh);
void shifting_destroy(shifting *sh);
coord *shifting_get_starting_coords(shifting *sh);
void shifting_set_starting_coords(shifting *sh, coord *c);
coord *shifting_get_ending_coords(shifting *sh);
void shifting_set_ending_coords(shifting *sh, coord *c);
void shifting_get_starting(shifting *sh, int *row, int *col);
void shifting_set_starting(shifting *sh, int row, int col);
void shifting_get_ending(shifting *sh, int *row, int *col);
void shifting_set_ending(shifting *sh, int row, int col);
void shifting_get_starting_color(shifting *sh, color *colr);
void shifting_set_starting_color(shifting *sh, color colr);
void shifting_get_ending_color(shifting *sh, color *colr);
bool shifting_get_got_through_loop(shifting *sh);
void shifting_set_ending_color(shifting *sh, color colr);
bool shifting_get_got_through_loop(shifting *sh);
void shifting_set_got_through_loop(shifting *sh, bool went_through_a_loop);
bool shifting_get_is_attack_move(shifting *sh);
void shifting_set_is_attack_move(shifting *sh, bool is_attack_move);
void shifting_get_all_coords(shifting *sh, int *f_row, int *f_col, int *t_row, int *t_col);
void shifting_set_all_coords(shifting *sh, int f_row, int f_col, int t_row, int t_col);
#endif
