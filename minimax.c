#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "minimax.h"
#include "game_funcs.h"
#include "misc_funcs.h"

/*
  Types of the structures are:
  SQUARE    : (INT row, INT col, STATE cur_state)
  SHIFTING  : (SQUARE square_i, SQUARE square_f, BOOL through_loop)
  BOARD     : (SQUARE board[6][6], INT nb_white, INT nb_black)
  P         : (BOARD surakarta, INT score, STACK SHIFTING previous_moves, STATE color)
  DIRECTION : ENUM of INT
  STATE     : ENUM of INT
  BOARD     : (SQUARE board[6][6], int nb_black, int nb_white)
 */

int score_evaluation(gamedata data) {
    /*
    This function is the most important one: it takes the P struct, and it
    outputs a score based on the board situation.
    The function can be tuned, and multiple functions can be written to get
    multiple AI.
     The used algorithm to calculate scores is fairly simple :
        - Watch if the game is won/lost, exit if it's the case
        - Count the number of remaining pieces for each player and score them
        - Add scores based on where the pieces are situated
        - Add scores based on whether the pieces are in an attack position
        - Add scores based on whether the pieces are in danger
    Evolutionary programming may be performed to get better values.
    We keep three values to manage board position scores, attack and defense
    modifiers.
    For cases values, due to geometric properties of the board,
    a lot of values can be merged:
    
    v1 v2 v2 v2 v2 v1
    v2 v3 v4 v4 v3 v2
    v2 v4 v3 v3 v4 v2
    v2 v4 v3 v3 v4 v2
    v2 v3 v4 v4 v3 v2
    v1 v2 v2 v2 v2 v1

    IN-OUT: P
     */

    int score = 0;
    int g = game_is_won(data);
    color *brd = data->board;

    if (g) {
        return (g * 65535);
    }

    bool *attacking = malloc(sizeof (bool) * 36);
    for (int i = 0; i < 36; attacking[i++] = 0);
    bool *attacked = malloc(sizeof (bool) * 36);
    for (int i = 0; i < 36; attacked[i++] = 0);
    coord case_p = malloc(sizeof (struct coord_s));
    for (int i = 0; i < 6; i++) {
        case_p->row = i;
        for (int j = 0; j < 6; j++) {
            case_p->col = j;
            get_attacks_pos(data, case_p, attacking, attacked);
        }
    }
    free(case_p);
    for (int i = 0; i < 36; i++) {
        color color_p = brd[i];
        if (color_p == data->current_player_color) {
            score += PIECE_VALUE;
            score += board_values[i];
            if (attacked[i]) {
                score -= DANGER_MODIFIER;
            }
            if (attacking[i]) {
                score += ATTACK_MODIFIER;
            }
        } else if (color_p != EMPTY) {
            score -= PIECE_VALUE;
            score -= board_values[i];
            if (attacked[i]) {
                score += DANGER_MODIFIER;
            }
            if (attacking[i]) {
                score -= ATTACK_MODIFIER;
            }
        }
    }
    free(attacking);
    free(attacked);
#ifdef DEBUG
    printf("----------------\n");
    //#ifdef REALLY_DEBUG
    print_surakarta(data);
    //#endif
    print_stack_shifting(data->previous_moves);
    printf("score = %d\n", score);
    printf("----------------\n");
#endif
    return (score);
}

shifting_s minimax_root(gamedata data) {
    stack_shifting *first_moves = stack_shifting_new();
    get_next_moves(data, &first_moves);
    int best_score = -65535;
    shifting_s best_move = NULL;
    while (!stack_shifting_is_empty(first_moves)) {
        shifting_s move = stack_shifting_pop(&first_moves);
#ifdef DEBUG
        printf("FIRST MOVE [%s]: [%d][%d] -> [%d][%d]\n",
                ((move->is_attack_move) ? "ATTACK" : "MOVE"),
                move->f_row, move->f_col, move->t_row, move->t_col);
#endif
        next_turn(data, move);
        int score = -alphabeta(data, -65535, 65535, 1);
        undo_turn(data);
        if (score > best_score) {
            best_score = score;
            free(best_move);
            best_move = move;
        } else {
            free(move);
        }
    }
    return best_move;
}

int alphabeta(gamedata data, int alpha, int beta, int depth) {
    /*
    A very simple implementation of a minimax with alpha-beta pruning, based on
    the algorithm described on the dedicated French Wikipedia page:
    https://fr.wikipedia.org/wiki/%C3%89lagage_alpha-beta

    IN:  P data, INT alpha, INT beta, INT depth
    OUT: P best
     */
    stack_shifting *all_moves = stack_shifting_new();

    if (depth >= MAX_DEPTH || game_is_won(data)) {
        return (score_evaluation(data));
    }

    get_next_moves(data, &all_moves);

#ifdef FAIL_HARD
    int best = -65535;
#else
    int best = alpha;
#endif

    while (!stack_shifting_is_empty(all_moves)) {
        shifting_s move = stack_shifting_pop(&all_moves);
        next_turn(data, move);
        free(move);
#ifdef FAIL_HARD
        int score = -alphabeta(data, -beta, -MAX(best, alpha), depth + 1);
#else
        int score = -alphabeta(data, -beta, -alpha, depth + 1);
#endif
        if (score > best) {
            best = score;
            if (score >= alpha) {
                alpha = score;
                if (alpha >= beta) {
                    undo_turn(data);
                    break;
                }
            }
        }
        undo_turn(data);
    }
    while (!stack_shifting_is_empty(all_moves)) {
        free(stack_shifting_pop(&all_moves));
    }
    free(all_moves);
    return best;
}
