#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stack_coord.h"
#include "struct_funcs.h"

struct stack_coord_t {
    coord *head;
    struct stack_coord_t *tail;
};

stack_coord *stack_coord_new() {
    stack_coord *s = NULL;
    return (s);
}

stack_coord *stack_coord_copy(stack_coord *s) {
    stack_coord *ns = stack_coord_new();
    stack_coord *s_ptr = s;
    stack_coord_reverse(s);
    while (!(stack_coord_is_empty(s_ptr))) {
        stack_coord_push(ns, stack_coord_top(s_ptr));
        s_ptr = s_ptr->tail;
    }
    stack_coord_reverse(s);
    return (ns);
}

void stack_coord_destroy(stack_coord *s) {
    while (!stack_coord_is_empty(s)) {
        coord_destroy(stack_coord_pop(s));
    }
}

bool stack_coord_is_empty(stack_coord *s) {
    return (s == NULL);
}

void stack_coord_push(stack_coord *s, coord *e) {
    stack_coord *hd = malloc(sizeof (stack_coord));
    hd->head = e;
    hd->tail = s;
    s = hd;
}

coord *stack_coord_pop(stack_coord *s) {
    stack_coord *to_del = s;
    if (stack_coord_is_empty(s)) {
        printf("Cannot pop out the head of an empty stack!\n");
        exit(EXIT_FAILURE);
    }
    coord *e = s->head;
    s = s->tail;
    free(to_del);
    to_del = NULL;
    return (e);
}

coord *stack_coord_top(stack_coord *s) {
    return (s->head);
}

void stack_coord_reverse(stack_coord *s) {
    stack_coord *ns = stack_coord_new();
    while (!stack_coord_is_empty(s)) {
        stack_coord_push(ns, stack_coord_pop(s));
    }
    s = ns;
}

void stack_coord_rev_append(stack_coord *s1, stack_coord *s2) {
    stack_coord *s_ptr = s1;
    while (!(stack_coord_is_empty(s_ptr))) {
        stack_coord_push(s2, stack_coord_top(s_ptr));
        s_ptr = s_ptr->tail;
    }
}

void stack_coord_append(stack_coord *s1, stack_coord *s2) {
    stack_coord *s_ptr = s1;
    stack_coord_reverse(s1);
    while (!(stack_coord_is_empty(s_ptr))) {
        stack_coord_push(s2, stack_coord_top(s_ptr));
        s_ptr = s_ptr->tail;
    }
    stack_coord_reverse(s1);
}

