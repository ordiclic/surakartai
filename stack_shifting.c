#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stack_shifting.h"
#include "shifting.h"
#include "struct_funcs.h"

struct stack_shifting_t {
    shifting *head;
    struct stack_shifting_t *tail;
};

stack_shifting *stack_shifting_new() {
    stack_shifting *s = NULL;
    return (s);
}

stack_shifting *stack_shifting_copy(stack_shifting *s) {
    stack_shifting *ns = stack_shifting_new();
    stack_shifting *s_ptr = s;
    stack_shifting_reverse(s);
    while (!(stack_shifting_is_empty(s_ptr))) {
        stack_shifting_push(ns, stack_shifting_top(s_ptr));
        s_ptr = s_ptr->tail;
    }
    stack_shifting_reverse(s);
    return (ns);
}

void stack_shifting_destroy(stack_shifting *s) {
    while (!stack_shifting_is_empty(s)) {
        shifting_destroy(stack_shifting_pop(s));
    }
}

bool stack_shifting_is_empty(stack_shifting *s) {
    return (s == NULL);
}

void stack_shifting_push(stack_shifting *s, shifting *e) {
    stack_shifting *hd = malloc(sizeof (stack_shifting));
    hd->head = e;
    hd->tail = s;
    s = hd;
}

shifting *stack_shifting_pop(stack_shifting *s) {
    stack_shifting *to_del = s;
    if (stack_shifting_is_empty(s)) {
        printf("Cannot pop out the head of an empty stack!\n");
        exit(EXIT_FAILURE);
    }
    shifting *e = s->head;
    s = s->tail;
    free(to_del);
    to_del = NULL;
    return (e);
}

shifting *stack_shifting_top(stack_shifting *s) {
    return (s->head);
}

void stack_shifting_reverse(stack_shifting *s) {
    stack_shifting *ns = stack_shifting_new();
    while (!stack_shifting_is_empty(s)) {
        stack_shifting_push(ns, stack_shifting_pop(s));
    }
    s = ns;
}

void stack_shifting_rev_append(stack_shifting *s1, stack_shifting *s2) {
    stack_shifting *s_ptr = s1;
    while (!(stack_shifting_is_empty(s_ptr))) {
        stack_shifting_push(s2, stack_shifting_top(s_ptr));
        s_ptr = s_ptr->tail;
    }
}

void stack_shifting_append(stack_shifting *s1, stack_shifting *s2) {
    stack_shifting *s_ptr = s1;
    stack_shifting_reverse(s1);
    while (!(stack_shifting_is_empty(s_ptr))) {
        stack_shifting_push(s2, stack_shifting_top(s_ptr));
        s_ptr = s_ptr->tail;
    }
    stack_shifting_reverse(s1);
}
