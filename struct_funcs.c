#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "struct_funcs.h"

const player_type players[2] = {HUMAN, COMPUTER};

direction directions_capture[4] = {UP, RIGHT, DOWN, LEFT};

const int directions_move[][2] = {
    {-1, -1},
    {-1, 0},
    {-1, 1},
    { 0, -1},
    { 0, 1},
    { 1, -1},
    { 1, 0},
    { 1, 1}
};



const int board_values[36] = {
    v1, v2, v2, v2, v2, v1,
    v2, v3, v4, v4, v3, v2,
    v2, v4, v3, v3, v4, v2,
    v2, v4, v3, v3, v4, v2,
    v2, v3, v4, v4, v3, v2,
    v1, v2, v2, v2, v2, v1
};
