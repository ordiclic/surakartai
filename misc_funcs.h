/*
 * File:   misc_funcs.h
 * Author: ordiclic
 *
 * Created on 19 mai 2015, 16:02
 */

#ifndef MISC_FUNCS_H
#define	MISC_FUNCS_H

#include "struct_funcs.h"

void print_surakarta(gamedata data);
void print_stack_shifting(stack_shifting * s_sh);
char* color_to_str(color s);
void flush_line();
bool input_move(gamedata data, shifting_s sh);
char* dir_to_str(direction d);


#endif
