#include <stdio.h>
#include <stdlib.h>
#include "struct_funcs.h"
#include "minimax.h"
#include "misc_funcs.h"
#include "game_funcs.h"

int main() {
    gamedata data = gamedata_init();
    data->current_player_color = WHITE;
    print_surakarta(data);
    while (!game_is_won(data)) {
        stack_empty_shifting(&data->previous_moves);
        shifting_s best_move = minimax_root(data);
        printf("Player %s playing %s: [%d][%d] (%s) -> [%d][%d] (%s)\n",
                color_to_str(data->current_player_color),
                ((best_move->is_attack_move) ? "ATTACK" : "MOVE"),
                best_move->f_row, best_move->f_col,
                color_to_str(data->board[IDX(best_move->f_row, best_move->f_col)]),
                best_move->t_row, best_move->t_col,
                color_to_str(data->board[IDX(best_move->t_row, best_move->t_col)])
                );
        next_turn(data, best_move);
        free(best_move);
        print_surakarta(data);
    }
}
