#ifndef MINIMAX_H
#define	MINIMAX_H

#include "struct_funcs.h"

shifting minimax_root(gamedata data);
int score_evaluation(gamedata data);
int alphabeta(gamedata data, int alpha, int beta, int depth);

#endif
