#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "misc_funcs.h"
#include "struct_funcs.h"
#include "game_funcs.h"

void print_surakarta(gamedata data) {
    color *b = data->board;
    /*
      + + + + + +
      + O + X + +
      + + + + + +
      + X + + + +
      + + X + + +
      + + + + + +
     */
    printf("\n");
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) {
            printf(" %s", color_to_str(b[IDX(i, j)]));
        }
        printf("\n");
    }
    printf("\n");
}

void print_stack_shifting(stack_shifting * s_sh) {
    stack_shifting *ptr = s_sh;
    printf("stack_shifting contents: \n");
    while (!(stack_shifting_is_empty(ptr))) {
        shifting_s sh = ptr->head;
        printf("[%d][%d] (%s) -> [%d][%d] (%s) [%s]\n",
                sh->f_row, sh->f_col, color_to_str(sh->color_from),
                sh->t_row, sh->t_col, color_to_str(sh->color_to),
                ((sh->is_attack_move) ? "ATTACK" : "MOVE")
                );
        ptr = ptr->tail;
    }
    printf("--------\n");
}

char* color_to_str(color c) {
    switch (c) {
        case WHITE:
            return ("O");
        case BLACK:
            return ("X");
        case EMPTY:
            return ("+");
        default:
            return ("~");
    }
}

char* dir_to_str(direction d) {
    switch (d) {
        case UP:
            return ("U");
        case DOWN:
            return ("D");
        case LEFT:
            return ("L");
        case RIGHT:
            return ("R");
        default:
            return ("§");
    }
}

void flush_line() {
    while (getchar() != '\n');
}

bool input_move(gamedata data, shifting_s sh) {
    char *s_in;
    unsigned long s_len = 0;
    char from[3], to[3];
    getline(&s_in, &s_len, stdin);
    sscanf(s_in, "%2s-%2s", from, to);
    flush_line();
    sh->f_row = (int) (from[0]) - 97;
    sh->f_col = (int) (from[1]) - 48;
    sh->t_row = (int) (to[0]) - 97;
    sh->t_col = (int) (to[1]) - 48;
    return (is_legal_move(data, sh));
}
