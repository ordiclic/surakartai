CC		= gcc
override CFLAGS := -Wall $(CFLAGS) -Werror -pedantic -g -std=c99 -O3
SOURCES = board.c coord.c gamedata.c game_funcs.c main.c minimax.c misc_funcs.c shifting.c stack_coord.c stack_shifting.c struct_funcs.c
OBJECTS = $(SOURCES:.c=.o)
HEADERS = $(SOURCES:.c=.h)
EXE = surakarta

$(EXE): $(OBJECTS)
	$(CC) -o $(EXE) $(OBJECTS)

check:
	$(CC) -Wall -Wextra -fsyntax-only -pedantic -std=c99 *.c

clean:
	rm -f $(EXE)
	rm -f *~
	rm -f $(OBJECTS)
	rm -f \#*\#
	rm -f vgcore.*
	rm -f callgrind.*
	rm -f $(EXE_TEST_LOADSAVE)
	rm -f a.out
	rm -f *.gch

tar: clean
	rm -f ../$(EXE).tar
	tar -cf ../$(EXE).tar ./ --exclude='.git' --exclude="./Doc Divers" --exclude="./nbproject" --exclude="./save"

test_loadsave:
	$(CC) -o $(EXE_TEST_LOADSAVE) test_loadsave.c stack.c options.c -g
