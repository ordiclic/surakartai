#ifndef STACK_SHIFTING_H
#define	STACK_SHIFTING_H

#include <stdbool.h>
#include "shifting.h"
#include "struct_funcs.h"

typedef struct stack_shifting_t stack_shifting;

stack_shifting *stack_shifting_new();
bool stack_shifting_is_empty(stack_shifting *s);
void stack_shifting_push(stack_shifting *s, shifting *e);
shifting *stack_shifting_pop(stack_shifting *s);
shifting *stack_shifting_top(stack_shifting *s);
void stack_shifting_destroy(stack_shifting *s);
void stack_shifting_reverse(stack_shifting *s);
stack_shifting *stack_shifting_copy(stack_shifting *s);
void stack_shifting_rev_append(stack_shifting *s1, stack_shifting *s2);


#endif	/* STACK_SHIFTING_H */

