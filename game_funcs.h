#ifndef GAME_FUNCS_H
#define	GAME_FUNCS_H

#include "struct_funcs.h"

bool dist_is_one_between(int f_row, int f_col, int t_row, int t_col);
bool is_starting_position(shifting *cursor, direction orig_dir, direction cur_dir);
bool advance_cursor(direction *dir, shifting *sh_cursor);
void get_attacks_pos(gamedata *data, coord *case_p, bool *attacking, bool *attacked);
void get_attacks(gamedata data, coord case_p, stack_shifting ** attacks);
bool is_attacked_piece(coord case_p, stack_shifting * list_of_attacks);
bool is_attacking_piece(coord case_p, stack_shifting * list_of_attacks);
int game_is_won(gamedata data);
stack_coord *positions(gamedata data, color c);
void add_simple_legal_moves(gamedata data, coord square_i, stack_shifting ** all_moves);
void get_next_moves(gamedata data, stack_shifting ** all_moves);
void undo_turn(gamedata data);
void next_turn(gamedata data, shifting one_move);
bool is_legal_move(gamedata data, shifting sh);


#endif	/* GAME_FUNCS_H */

