#include "gamedata.h"
#include "struct_funcs.h"
#include "board.h"
#include "stack_shifting.h"

struct gamedata_s {
    board *board;
    stack_shifting *previous_moves;
    color current_player_color;
};

gamedata *gamedata_new() {
    gamedata *data = malloc(sizeof (gamedata));
    data->previous_moves = stack_shifting_new();
    data->board = board_new();
    return (data);
}

gamedata *gamedata_copy(gamedata *data) {
    gamedata *new_data = malloc(sizeof (gamedata));
    new_data->previous_moves = stack_shifting_copy(data->previous_moves);
    new_data->board = board_copy(data->board);
}

void gamedata_destroy(gamedata *data) {
    board_destroy(data->board);
    stack_shifting_destroy(&data->previous_moves);
    free(data);
}

board *gamedata_get_board(gamedata *data) {
    return (data->board);
}

stack_shifting *gamedata_get_previous_moves(gamedata *data) {
    return (data->previous_moves);
}