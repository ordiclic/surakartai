#ifndef STRUCT_FUNCS_H
#define	STRUCT_FUNCS_H

#include <stdbool.h>

#define WHITE_PLAYER HUMAN
#define BLACK_PLAYER COMPUTER


#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define IDX(ROW, COL) ((ROW) * 6 + (COL))

enum color_e {
    WHITE, BLACK, EMPTY
};
typedef enum color_e color;

enum player_e {
    HUMAN, COMPUTER
};
typedef enum player_e player_type;

enum direction_e {
    RIGHT, LEFT, UP, DOWN
};
typedef enum direction_e direction;


#define v1 1
#define v2 3
#define v3 5
#define v4 3
#define DANGER_MODIFIER -10
#define ATTACK_MODIFIER 25
#define PIECE_VALUE 50
#define MAX_DEPTH 4

extern const player_type players[2];
extern direction directions_capture[4];
extern const int directions_move[][2];
extern const int board_values[36];

void surakarta_init(color * board);

#endif

