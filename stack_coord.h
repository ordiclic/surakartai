#ifndef STACK_COORD_H
#define	STACK_COORD_H

#include <stdbool.h>
#include "coord.h"
#include "struct_funcs.h"

typedef struct stack_coord_t stack_coord;

stack_coord *stack_coord_new();
bool stack_coord_is_empty(stack_coord *s);
void stack_coord_push(stack_coord *s, coord *e);
coord *stack_coord_pop(stack_coord *s);
coord *stack_coord_top(stack_coord *s);
void stack_coord_destroy(stack_coord *s);
void stack_coord_reverse(stack_coord *s);
stack_coord *stack_coord_copy(stack_coord *s);
void stack_coord_rev_append(stack_coord *s1, stack_coord *s2);

#endif

