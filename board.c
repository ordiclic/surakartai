#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "board.h"

board *board_new() {
    board b = malloc(sizeof (board) * 36);
    for (int i = 0; i < 12; i++) {
        b[i] = WHITE;
    }
    for (int i = 12; i < 24; i++) {
        b[i] = EMPTY;
    }
    for (int i = 24; i < 36; i++) {
        b[i] = BLACK;
    }
    return (b);
}

board *board_copy(board b) {
    board b2 = malloc(sizeof (board) * 36);
    memcpy(b2, b, sizeof (board) * 36);
    return (b2);
}

void board_destroy(board *b) {
    free(b);
}

color board_get_from_coords(board *b, coord *c) {
    int row = c->row;
    int col = c->col;
    if (row < 0 || row > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "row index out of bounds (%d).\n", row);
        exit(EXIT_FAILURE);
    }
    if (col < 0 || col > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "col index out of bounds (%d).\n", col);
        exit(EXIT_FAILURE);
    }
    return (b[6 * row + col]);
}

void board_set_from_coords(board *b, coord *cd, color c) {
    int row = cd->row;
    int col = cd->col;
    if (row < 0 || row > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "row index out of bounds (%d).\n", row);
        exit(EXIT_FAILURE);
    }
    if (col < 0 || col > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "col index out of bounds (%d).\n", col);
        exit(EXIT_FAILURE);
    }
    b[6 * row + col] = c;
}

color board_get_from_index(board *b, int idx) {
    if (idx < 0 || idx > 35) {
        fprintf(stderr, "Error: board_get_from_index(board *b, int idx: ");
        fprintf(stderr, "index %d out of bounds.\n",);
        exit(EXIT_FAILURE);
    }
    return (b[idx]);
}

color board_set_from_index(board *b, int idx, color c) {
    if (idx < 0 || idx > 35) {
        fprintf(stderr, "Error: board_get_from_index(board *b, int idx: ");
        fprintf(stderr, "index %d out of bounds.\n",);
        exit(EXIT_FAILURE);
    }
    b[idx] = c;
}

color board_get_from_indexes(board *b, int row, int col) {
    if (row < 0 || row > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "row index out of bounds (%d).\n", row);
        exit(EXIT_FAILURE);
    }
    if (col < 0 || col > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "col index out of bounds (%d).\n", col);
        exit(EXIT_FAILURE);
    }
    return (b[6 * row + col]);
}

color board_set_from_indexes(board *b, int row, int col, color c) {
    if (row < 0 || row > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "row index out of bounds (%d).\n", row);
        exit(EXIT_FAILURE);
    }
    if (col < 0 || col > 5) {
        fprintf(stderr, "Error: board_get_from_coords: ");
        fprintf(stderr, "col index out of bounds (%d).\n", col);
        exit(EXIT_FAILURE);
    }
    b[6 * row + col] = c;
}
