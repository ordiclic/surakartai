#ifndef COORD_H
#define	COORD_H

typedef struct coord_s coord;
coord *coord_new();
coord *coord_copy(coord *cd);
void coord_destroy(coord *cd);
int coord_get_row(coord *cd);
void coord_set_row(coord *cd, int row);
int coord_get_col(coord *cd);
void coord_set_col(coord *cd, int col);

#endif

