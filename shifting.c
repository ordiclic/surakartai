#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "shifting.h"
#include "coord.h"

struct shifting_s {
    int f_row;
    int f_col;
    int t_row;
    int t_col;
    color color_from;
    color color_to;
    bool through_loop;
    bool is_attack_move;
};

shifting *shifting_new() {
    shifting *sh = malloc(sizeof (shifting));
    memset(sh, 0, sizeof (shifting));
    return (sh);
}

shifting *shifting_copy(shifting *sh) {
    shifting *sh_copy = malloc(sizeof (shifting));
    memcpy(sh_copy, sh, sizeof (shifting));
    return (sh_copy);
}

void shifting_destroy(shifting *sh) {
    free(sh);
}

coord *shifting_get_starting_coords(shifting *sh) {
    coord *c = coord_new();
    coord_set_row(c, sh->f_row);
    coord_set_col(c, sh->f_col);
    return (c);
}

void shifting_set_starting_coords(shifting *sh, coord *c) {
    sh->f_row = coord_get_row(c);
    sh->f_col = coord_get_col(c);
}

coord *shifting_get_ending_coords(shifting *sh) {
    coord *c = coord_new();
    coord_set_row(c, sh->t_row);
    coord_set_col(c, sh->t_col);
    return (c);
}

void shifting_set_ending_coords(shifting *sh, coord *c) {
    sh->t_row = coord_get_row(c);
    sh->t_col = coord_get_col(c);
}

void shifting_get_starting(shifting *sh, int *row, int *col) {
    *row = sh->f_row;
    *col = sh->f_col;
}

void shifting_set_starting(shifting *sh, int row, int col) {
    sh->f_row = row;
    sh->f_col = col;
}

void shifting_get_ending(shifting *sh, int *row, int *col) {
    *row = sh->t_row;
    *col = sh->t_col;
}

void shifting_set_ending(shifting *sh, int row, int col) {
    sh->t_row = row;
    sh->t_col = col;
}

void shifting_get_starting_color(shifting *sh, color *colr) {
    *colr = sh->color_from;
}

void shifting_set_starting_color(shifting *sh, color colr) {
    sh->color_from = colr;
}

void shifting_get_ending_color(shifting *sh, color *colr) {
    *colr = sh->color_to;
}

void shifting_set_ending_color(shifting *sh, color colr) {
    sh->color_to = colr;
}

bool shifting_get_got_through_loop(shifting *sh) {
    return (sh->through_loop);
}

void shifting_set_got_through_loop(shifting *sh, bool went_through_a_loop) {
    sh->through_loop = went_through_a_loop;
}

bool shifting_get_is_attack_move(shifting *sh) {
    return (sh->is_attack_move);
}

void shifting_set_is_attack_move(shifting *sh, bool is_attack_move) {
    sh->is_attack_move = is_attack_move;
}

void shifting_get_all_coords(shifting *sh, int *f_row, int *f_col, int *t_row, int *t_col) {
    *f_row = sh->f_row;
    *f_col = sh->f_col;
    *t_row = sh->t_row;
    *t_col = sh->t_col;
}

void shifting_set_all_coords(shifting *sh, int f_row, int f_col, int t_row, int t_col) {
    sh->f_row = f_row;
    sh->f_col = f_col;
    sh->f_row = t_row;
    sh->f_col = t_col;
}